#include "rotation.h"
#include "image.h"
#include <stdint.h>

struct image rotate(struct image const source) {
    struct image rotated_image = create_img(source.height, source.width);
     for (size_t i = 0; i < source.height; ++i) {
        for (size_t j = 0; j < source.width; ++j) {
            rotated_image.data[rotated_image.width * j + rotated_image.width - i - 1] = source.data[source.width * i + j];
        }
    }
    
    return rotated_image;
}
