#include "image.h"
#include <malloc.h>

struct image create_img(size_t width, size_t height) {
    struct image result = (struct image) { 
        .width=width, 
    	.height=height,
    	.data=malloc(sizeof(struct pixel) * width * height) 
    };

    return result;
}

void remove_img(struct image *img) {
    free(img->data);
}
