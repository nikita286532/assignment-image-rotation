#define _CRT_SECURE_NO_DEPRECATE
#include "io.h"
#include <stddef.h>
#include <stdio.h>

enum open_status open_file(int argc, char **argv, char **in_path, char **out_path, FILE **in_file, FILE **out_file) {
    if (argc != 3) {
        return INVALID_ARGS;
    }
    *in_path = argv[1];
    *out_path = argv[2];

    *in_file = fopen(*in_path, "r");
    if (*in_file == NULL) { return FAILED_TO_OPEN_INPUT_FILE; }
    *out_file = fopen(*out_path, "w");
    if (*out_file == NULL) { return FAILED_TO_OPEN_OUTPUT_FILE; }

    return OPEN_SUCCESS;
}

enum close_status close_file(FILE **in_file, FILE **out_file, struct image *img1, struct image *img2) {
    if (fclose(*in_file)) {
        return FAILED_TO_CLOSE_INPUT_FILE;
    }
    if (fclose(*out_file)) {
        return FAILED_TO_CLOSE_OUTPUT_FILE;
    }

    remove_img(img1);
    remove_img(img2);

    return CLOSE_SUCCESS;
}
