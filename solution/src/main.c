#include "io.h"
#include "bmp.h"
#include "rotation.h"
#include <stddef.h>

int main( int argc, char** argv ) {
    
    char *input_path = NULL;
    char *output_path = NULL;
    FILE *input_pFile = NULL;
    FILE *output_pFile = NULL;

    switch (open_file(argc, argv, &input_path, &output_path, &input_pFile, &output_pFile)) {
        case INVALID_ARGS:
            fprintf(stderr, "INVALID_ARGS\n");
            return 1;
        case FAILED_TO_OPEN_INPUT_FILE:
            fprintf(stderr, "FAILED_TO_OPEN_INPUT_FILE\n");
            return 1;
        case FAILED_TO_OPEN_OUTPUT_FILE:
            fprintf(stderr, "FAILED_TO_OPEN_OUTPUT_FILE\n");
            return 1;
        case OPEN_SUCCESS:
            printf("OPEN_SUCCESS\n");
    }

    struct image img;
    enum read_status r_status = from_bmp(input_pFile, &img);
    if (r_status) {
        return 1;
    }

    struct image rotated_img = rotate(img);
    enum write_status w_status = to_bmp(output_pFile, &rotated_img);
    if (w_status) {
        return 1;
    }

    switch (close_file(&input_pFile, &output_pFile, &img, &rotated_img)) {
        case FAILED_TO_CLOSE_INPUT_FILE:
            fprintf(stderr, "FAILED_TO_CLOSE_INPUT_FILE\n");
            return 1;
        case FAILED_TO_CLOSE_OUTPUT_FILE:
            fprintf(stderr, "FAILED_TO_CLOSE_OUTPUT_FILE\n");
            return 1;
        case CLOSE_SUCCESS:
            printf("CLOSE_SUCCESS");
    }

    return 0;
}
