#include "bmp.h"
#include "stddef.h"
#include <malloc.h>

static const uint16_t Bf_Type = 0x4D42;
static const uint16_t Bit_Count = 24;
static const uint32_t Bi_Size = 40;

enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in || !img) { return READ_ERROR; }

    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, in);
    if (header.bfType > Bf_Type || header.bfType <= 0) { return READ_INVALID_SIGNATURE; }
    if (header.biBitCount != Bit_Count) { return READ_INVALID_BITS; }
    if (header.biSize != Bi_Size) { return READ_INVALID_HEADER; }

    *img = create_img(header.biWidth, header.biHeight);

    size_t padding = 4 - img->width * (Bit_Count / 8) % 4;

    for (int i = 0; i < img->height; i++) {
        fread((char *) img->data + i * img->width * 3, 3, img->width, in);
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (!out || !img) { return WRITE_ERROR; }

    size_t padding = 4 - img->width * (Bit_Count / 8) % 4;

    uint32_t biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    struct bmp_header header = {
        .bfType=Bf_Type,
        .biSizeImage= biSizeImage,
        .bfileSize = biSizeImage + sizeof(struct bmp_header),
        .bOffBits=sizeof(struct bmp_header),
        .biSize=Bi_Size,
        .biWidth=img->width,
        .biHeight=img->height,
        .biPlanes=1,
        .biBitCount=Bit_Count,
    };

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    fseek(out, header.bOffBits, SEEK_SET);

    uint8_t *arr_padd = malloc(padding);

    if (!arr_padd) { return WRITE_ERROR; }

    for (size_t i = 0; i < padding; i++) {
        *(arr_padd + i) = 0;
    }

    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, out);
            fwrite(arr_padd, padding, 1, out);
        }
    }

    free(arr_padd);

    return WRITE_OK;
}
