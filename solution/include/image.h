#ifndef IMAGE_H
#define IMAGE_H

#include "pixel.h"
#include "stddef.h"
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_img(size_t width, size_t height);
void remove_img(struct image* img);

#endif // IMAGE_H
