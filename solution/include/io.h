#ifndef IO_H
#define IO_H

#include "image.h"
#include <stdio.h>

enum open_status {
    OPEN_SUCCESS = 0,
    INVALID_ARGS,
    FAILED_TO_OPEN_INPUT_FILE,
    FAILED_TO_OPEN_OUTPUT_FILE
};

enum close_status {
    CLOSE_SUCCESS = 0,
    FAILED_TO_CLOSE_INPUT_FILE,
    FAILED_TO_CLOSE_OUTPUT_FILE
};

enum open_status open_file(int argc, char **argv, char **in_path, char **out_path, FILE **in_file, FILE **out_file);

enum close_status close_file(FILE **in_file, FILE **out_file, struct image *img1, struct image *img2);

#endif // IO_H
