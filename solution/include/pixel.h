#ifndef PIXEL_H
#define PIXEL_H

#include <stdint.h>

struct pixel { 
	uint8_t r;
	uint8_t g; 
	uint8_t b; 
};

#endif // PIXEL_H
